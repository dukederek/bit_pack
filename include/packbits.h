#ifndef PACKBITS_H
#define PACKBITS_H

#include <string>
#include <vector>

class Bit_Packer
{
    public:
        Bit_Packer();
        ~Bit_Packer();

        // Container for the bits
        std::vector<uint8_t> packed_Bits;

        // Take "binary" data in which is encoded as one bit per byte.
        // I.E. only values in are 0x00 and 0x01
        int pack_Bits(std::vector<uint8_t> data_In);

        int save_Bits(std::string filename);

    private:

};

std::vector<uint8_t> Make_Sample(int len);

#endif // PACKBITS_H

#include "packbits.h"

#include <iostream>
#include <vector>

int main(){
    // Make some sample random data
    std::vector<uint8_t> rvec = Make_Sample(128);

    Bit_Packer packer;

    // Load the bits into the packer
    packer.pack_Bits(rvec);

    // Export the bits into the file.
    packer.save_Bits("test_Out.bin");

    return 0;
}

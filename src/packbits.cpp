#include "packbits.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

Bit_Packer::Bit_Packer()
{
}

Bit_Packer::~Bit_Packer()
{
}

int Bit_Packer::pack_Bits(std::vector<uint8_t> data_In){
    // The output vector is 8 times smaller than the input vector (rounded up)
    int packed_Size = (data_In.size() / 8) + (data_In.size() % 8 != 0);
    // Empty the container
    packed_Bits.resize(packed_Size, 0);

    // Check the values in data_In are only 0 or 1
    bool data_Good = std::all_of(data_In.begin(), data_In.end(), [](uint8_t val){return (val == 0) || (val == 1);});
    if (!data_Good){
        std::cout << "Not all data is 0 or 1\n";
        return 1;
    }
    else{
        std::cout << "Data good\n";
    }

    // Keep track of the byte and the bit number in the byte.
    // When bit_Num gets to 7, set back to 0 and increment
    // byte_Num.
    int bit_Num = 0;
    int byte_Num = 0;
    for(auto bit : data_In){
        // Shift the bit value into the right position and add to the byte.
        packed_Bits[byte_Num] += bit << (7 - bit_Num);

        // Increment the bit/byte numbers to go to the next bit.
        if (bit_Num < 7){
            bit_Num++;
        }
        else{
            // This byte is finished now, go to (the beginning of) the next one.
            bit_Num = 0;
            byte_Num++;
        }
    }
    return 0;
}

int Bit_Packer::save_Bits(std::string filename){
    // Open a file output
    std::ofstream file_Out(filename, std::ofstream::binary);

    // Fill the file
    for(auto byte:packed_Bits){
        file_Out << byte;
    }
    // Close the file.
    file_Out.close();

    return 0;
}

std::vector<uint8_t> Make_Sample(int len){
    std::vector<uint8_t> rand_vec(len);
    for (auto& val : rand_vec){
        val = std::rand() % 2;
    }

    return rand_vec;
}
